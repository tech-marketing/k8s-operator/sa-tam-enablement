# The GitLab Kubernetes Operator enablement for SAs and TAMs

This project contains the enablement assets on the GitLab Kubernetes Operator for SAs and TAMs.

To get started, go to [instructions](https://gitlab.com/tech-marketing/k8s-operator/sa-tam-enablement/-/blob/main/instructions.md).
