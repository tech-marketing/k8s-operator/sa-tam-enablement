# The GitLab Kubernetes Operator installation on a local kind K8s cluster

This enablement covers a vanilla Kubernetes distribution, namely [kind](https://kind.sigs.k8s.io/). One of the goals of this enablement is for you to be able to run through the installation of the GitLab K8s Operator on your own GitLab-provisioned MacBook Pro (with an 8-core CPU and 32GB of RAM). Although, we will guide you through the installation of **kind** in the steps below, a good resource for its installation can be found [here](https://docs.gitlab.com/charts/development/kind/). In the future, enablement for the deployment of the GitLab K8s Operator to an OpenShift cluster will be developed.

## Installing the GitLab Kubernetes Operator

Let's go over the steps to install the GitLab Kubernetes Operator.

> **NOTE**: Please refer to the [GitLab Kubernetes Operator project](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator) for more in-depth information.

### Prerequisites

You need to install the following packages as prerequisites of this lab:

0. Ensure you have **Homebrew** installed on your Mac. If you don't, please follow the [instructions](https://brew.sh/) in the Homebrew landing page to install it.

> **NOTE**: you can verify if you already have Homebrew on your Mac by entering the command **brew --version** from a command window. If you already have Homebrew installed and would like to upgrade it to its latest release, enter the command **brew upgrade** from a command window.

1. Ensure you have **Docker Desktop** installed on your Mac. If you don't, please download and install by following these [instructions](https://docs.docker.com/desktop/mac/install/). **VERY IMPORTANT:** Ensure to increase the Memory of your Docker Desktop to 8GB or greater

2. Ensure you have **helm** installed on your Mac. If you don't, please install it on your Mac by opening a command window and pasting the following command into it:

> brew install helm

3. Ensure you have **kubectl** installed on your Mac. if you already installed the kubectl client from AWS, Azure, or Google then you can skip this step. If you don't have **kubectl** on your Mac, please install it on your Mac by opening a command window and pasting the following command into it:

> brew install kubernetes-cli

4. Ensure you have **kind** installed on your Mac. **kind** is a tool for running local Kubernetes clusters using Docker container *nodes*. kind may be used for local development.If you don't have **kind** on your Mac, please install it on your Mac by opening a command window and pasting the following command into it:

> brew install kind

### Creating script to create K8s, install operator and instantiate a GitLab instance

Now let's create a script on your local drive that will execute all the necessary steps to create a **kind** K8s cluster on your laptop/desktop, deploy the GitLab K8s Operator to it and then instantiate a GitLab instance on your local K8s cluster.

5. Open a command window on your laptop/desktop and go to your $HOME directory

6. Create a subdirectory in your $HOME directory and name is **gl-op-install**:

> mkdir gl-op-install

7. Change directory to this newly created subdirectory:

> cd gl-op-install

8. Using your favorite editor, create a new file called **gl_kind.sh** inside subdirectory **gl-op-install**. Then paste the following content into file **gl_kind.sh**:

```
#!/bin/sh

# This script automates the creation of a kind cluster on your laptop/desktop, deploys the GitLab K8s Operator to it, and finally it instantiates an instance of GitLab on the cluster by using the Operator
# IMPORTANT: Ensure to increase the Memory of your Docker Desktop to 8GB or greater

# Execute this script from a directory on your laptop/desktop for which you have write access
# This script is for a macOS 10.15.7 (or later) laptop/desktop with 32GB of RAM and 8-core Intel Core i9 CPU

# Prerequisites
# For this script to work, you need to have the following software installed on your laptop/desktop:
# 1. Docker Desktop
# 2. Helm
# 3. Kubectl
# 4. kind - tool to run a local K8s cluster using Docker container nodes
# 5. Access to the internet

# Variable definition section

# This variable is set to your laptop/desktop IP address. Most MacOS systems use en0 as the primary interface. If using a system with a different primary interface, please substitute that interface name for en0.
LOCAL_IP=$(ipconfig getifaddr en0)
# if for some reason LOCAL_IP is undefined, set it to an empty string
LOCAL_IP=${LOCAL_IP:-""}
# default the kind cluster name to "mykindcluster"
KIND_CLUSTER_NAME=${KIND_CLUSTER_NAME:-mykindcluster}
# the dir name where this script will place all downloaded and generated file will be the same as the cluster name
WORKING_DIR=${KIND_CLUSTER_NAME}
# default the GitLab K8s Operator version to the latest, which at the moment is "0.1.0"
GL_OPERATOR_VERSION=${GL_OPERATOR_VERSION:-"0.1.0"}
# default platform to "kubernetes" since kind cluster is considered vanilla K8s
PLATFORM=${PLATFORM:-"kubernetes"}
# this variable is used for cases where a runner needs to be installed - not used at the moment
RUNNER_TOKEN=${RUNNER_TOKEN:-""}

if [ -z "${LOCAL_IP}" ]
then
  echo "Missing required var LOCAL_IP. Define it in the environment or at the top of this script"
  echo " LOCAL_IP: ${LOCAL_IP}"
  exit 1
fi

create_working_dir(){
  mkdir ${WORKING_DIR}
  if [ $? -ne 0 ]
  then
    echo "Unable to create working subdirectory ${WORKING_DIR}. Please execute this script from a dir for which you have write access"
    exit 1
  fi
}

download_kind_ssl_chart(){
  # using the GitLab API to get the chart for the kind ssl configuration
  curl https://gitlab.com/api/v4/projects/3828396/repository/files/examples%2Fkind%2Fkind-ssl%2Eyaml/raw\?ref\=master > ${WORKING_DIR}/kind-ssl.yaml
}

create_cluster(){
  # Next, we create a kind cluster:
  kind create cluster --name=${KIND_CLUSTER_NAME} --config=${WORKING_DIR}/kind-ssl.yaml --image=kindest/node:v1.18.19
}

install_certmanager(){
  # Install certmanager per our docs:
  kubectl apply -f https://github.com/jetstack/cert-manager/releases/latest/download/cert-manager.yaml
}

create_gitlab_cert(){
  # Now we'll generate certificates for GitLab
  GITLAB_KEY_FILE=${GITLAB_KEY_FILE:-gitlab.key}
  GITLAB_CERT_FILE=${GITLAB_CERT_FILE:-gitlab.crt}
  GITLAB_HOST=${GITLAB_HOST:-*.${LOCAL_IP}.nip.io}

  openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ${WORKING_DIR}/${GITLAB_KEY_FILE} -out ${WORKING_DIR}/${GITLAB_CERT_FILE} -subj "/CN=${GITLAB_HOST}/O=${GITLAB_HOST}"
}

create_gitlab_system_namespace(){
  kubectl create namespace gitlab-system
}

deploy_gitlab_cert(){
  kubectl create secret -n gitlab-system tls custom-gitlab-tls --key=${WORKING_DIR}/${GITLAB_KEY_FILE} --cert=${WORKING_DIR}/${GITLAB_CERT_FILE}
}

create_pages_cert(){
  # Next we generate certificates for Pages
  PAGES_KEY_FILE=pages.key 
  PAGES_CERT_FILE=pages.crt 
  PAGES_HOST=${PAGES_HOST:-*.pages.${LOCAL_IP}.nip.io}
  openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ${WORKING_DIR}/${PAGES_KEY_FILE} -out ${WORKING_DIR}/${PAGES_CERT_FILE} -subj "/CN=${PAGES_HOST}/O=${PAGES_HOST}"
}

deploy_pages_cert(){
  kubectl create secret -n gitlab-system tls custom-pages-tls --key=${WORKING_DIR}/${PAGES_KEY_FILE} --cert=${WORKING_DIR}/${PAGES_CERT_FILE}
}

deploy_operator(){
  kubectl apply -f https://gitlab.com/api/v4/projects/18899486/packages/generic/gitlab-operator/${GL_OPERATOR_VERSION}/gitlab-operator-${PLATFORM}-${GL_OPERATOR_VERSION}.yaml
}

deploy_gitlab(){
# Next, we'll create a GitLab CR and apply it
cat > ${WORKING_DIR}/mygitlab.yaml << EOF_GITLAB
apiVersion: apps.gitlab.com/v1beta1
kind: GitLab
metadata:
  name: gitlab
spec:
  chart:
    version: "5.3.0"
    values:
      certmanager:
        install: false
      global:
        hosts:
          domain: ${LOCAL_IP}.nip.io
        ingress:
          configureCertmanager: false
          tls:
            secretName:  custom-gitlab-tls
        shell:
          port: 32022
      gitlab:
        gitlab-pages:
          ingress:
            tls:
              # You need to bring your own wildcard SSL certificate which covers
              # \`*.\<pages root domain\>\`. Create a k8s TLS secret with the name
              # \`custom-pages-tls\` with it.
              secretName: custom-pages-tls
        gitlab-shell:
          minReplicas: 1
          maxReplicas: 1
        webservice:
          minReplicas: 1
          maxReplicas: 1
      nginx-ingress:
        controller:
          service:
            nodePorts:
              # https port value below must match the KinD config file:
              #   nodes[0].extraPortMappings[0].containerPort
              https: 32443
          replicaCount: 1
          minAvailable: 1
        defaultBackend:
          replicaCount: 1
      registry:
        hpa:
          minReplicas: 1
          maxReplicas: 1
EOF_GITLAB

  kubectl -n gitlab-system apply -f ${WORKING_DIR}/mygitlab.yaml
}

# That's it - navigate to `https://gitlab.(your IP).nip.io` and log in with the root password.

# if you need to instantiate a runner then add a call to this function at then end of this script
install_runner(){
  # If a Runner is required, create a Secret with the selfsigned certificate and then deploy the runner chart (or Operator)
  kubectl -n gitlab-system create secret generic custom-runner-tls --from-file=gitlab.${LOCAL_IP}.nip.io.crt=${GITLAB_CERT_FILE}

  cat > runner.values.yaml << EOF_RUNNER
gitlabUrl: https://gitlab.${LOCAL_IP}.nip.io/
runnerRegistrationToken: ${RUNNER_TOKEN}
certsSecretName: custom-runner-tls
rbac:
  create: true
EOF_RUNNER

  helm upgrade --install -n gitlab-system gitlab-runner gitlab/gitlab-runner -f runner.values.yaml
}

create_working_dir
download_kind_ssl_chart
create_cluster
install_certmanager
create_gitlab_cert
echo ""
echo ""
read -n 1 -r -s -p "From a separate cmd window, run 'kubectl get pods --all-namespaces' and monitor certmanager pods. When they (3) are all up, press any key to continue"
echo ""
echo ""
create_gitlab_system_namespace
deploy_gitlab_cert
create_pages_cert
deploy_pages_cert
deploy_operator
echo ""
echo ""
read -n 1 -r -s -p "From a separate cmd window, run 'kubectl get pods --all-namespaces' and monitor gitlab-controller-manager pods. When it (1) is up, press any key to continue"
echo ""
echo ""
deploy_gitlab
echo ""
echo ""
read -n 1 -r -s -p "From a separate cmd window, run 'kubectl get pods -n gitlab-system' and monitor gitlab-webservice-default pods. When they (2) are up, press any key to continue"
echo ""
echo ""
GL_ROOT_PWD=$(kubectl -n gitlab-system get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo)
echo ""
echo ""
echo "Open an incognito browser window and point it to 'https://gitlab.${LOCAL_IP}.nip.io and log in as username root with password ${GL_ROOT_PWD}"
```
Save the file and exit your editor.

9. Make the newly created script executable:

> chmod +x gl_kind.sh

10. Execute the gl_kind.sh script

> ./gl_kind.sh

As the script executes, it will guide you through the process. At the end of the execution of the script, you will be provided with the URL, username and password to log on to your new GitLab instance that was deployed to your local **kind** K8s cluster.

### Appendix - gl-kind.sh notes

Here is a table that explains some of the main aspects of the **gl_kind.sh** script:

| Variable | Value to use |
| ---      | ---      |
| LOCAL_IP   | This variable is set by the script to your laptop/desktop IP address. Most MacOS systems use en0 as the primary interface. If using a system with a different primary interface, please substitute that interface name for en0. |
| KIND_CLUSTER_NAME   | This is the name that is given to your **kind** K8s cluster in Docker. The default name is *mykindcluster*. If you'd like to change it, edit the script and modify the default value to a name of your choosing |
| WORKING_DIR   | The script creates a few files that are used by it througout its execution. The files are created in this directory, whose default is the same as the name of the KIND_CLUSTER_NAME. If you'd like to change it, edit the script and modify the default value to a name of your choosing |
| GL_OPERATOR_VERSION   | This is version of the GitLab K8s Operator you'd like to use. To get the latest version, check out the [GitLab Operator releases page](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/releases). If you need to modify this variable, edit the script and modify its default value. |
| spec:chart:version   | In the deploy_gitlab() function, you may need to update the spec:chart:version field. This is the version of the GitLab K8s Operator [chart](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/charts/gitlab/charts/operator/Chart.yaml). Open the chart and use the version number for this variable |
| spec:chart:values:global:domain   | In the deploy_gitlab() function, the spec:chart:values:global:domain field is the domain for the GitLab instance and is set by the script. The domain uses the nip.io service, so your domain will be ${LOCAL_IP}.nip.io |
| create_working_dir()  | This function creates the subdirectory where generated files by the script will be stored |
| download_kind_ssl_chart  | This function downloads the Heml chart for the **kind ssl** configuration |
| create_cluster  | This function creates the **kind** K8s cluster on your laptop/desktop |
| install_certmanager | This function deploys the cert-manager to your **kind** K8s cluster on your laptop/desktop |
| create_gitlab_cert | This function creates the x.509 certificate to be used for SSL communication with the GitLab instance |
| create_gitlab_system_namespace | This function creates the gitlab-system namespace on your **kind** K8s cluster |
| deploy_gitlab_cert | This function creates a secret custom-gitlab-tls using the x.509 certificate created by create_gitlab_cert() |
| create_pages_cert | This function creates the x.509 certificate to be used for SSL communication with Pages sites hosted by this GitLab instance |
| deploy_pages_cert | This function creates a secret custom-pages-tls using the x.509 certificate created by create_pages_cert() |
| deploy_operator | This function deploys the GitLab K8s Operator to your **kind** K8s cluster on your laptop/desktop |
| deploy_gitlab | This function generates a Custom Resource and then deploys it to the **kind** K8s cluster, which then instantiates a GitLab instance on the cluster. This step takes about 5 minutes |

### References

The sources for this set of instructions are:

- [GitLab Operator Installation](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/blob/master/doc/installation.md)
- [Document how to deploy the Operator on KinD issue](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/issues/283)
- [Snippet by Dmytro Makovey](https://gitlab.com/-/snippets/2169261)
