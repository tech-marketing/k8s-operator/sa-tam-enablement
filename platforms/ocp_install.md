# The GitLab Kubernetes Operator installation on an OpenShift Container Platform (OCP) on Google Cloud Platform (GCP)

This enablement covers the [Red Hat OpenShift Container Platform](https://www.redhat.com/en/technologies/cloud-computing/openshift/container-platform) Kubernetes distribution, which is installed and run on Google Cloud Platform.

## Installing OCP on GCP

Most of the content in this section is based on the [OpenShift Cluster Setup instructions]( https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/blob/master/doc/openshift-cluster-setup.md#create-a-google-cloud-gcp-service-account).

### Prerequisites

They are:

1. You must have a GCP account. If you are a GitLab team member, provision your GCP account via the [GitLab Cloud Sandbox](https://gitlabsandbox.cloud) and sign in with your Okta account by following [these instructions](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#how-to-get-started).

2. You must have a Red Hat access account associated with your GitLab email. Contact our Red Hat Alliance liason; they will arrange to send you an account invitation email. Once you activate your Red Hat account, you will have access the to licenses and subscriptions needed to run OpenShift.

3. Ensure you have **helm** installed on your Mac. If you don't, please install it on your Mac by opening a command window and pasting the following command into it:

> brew install helm

### Enabling GCP APIs

If your GCP account is brand new, it will most likely have many APIs, that you need to carry out the subsequent steps, disabled. You need to enable them.

1. Login to the [GCP console](https://console.cloud.google.com/home/dashboard) and ensure to select your project (the one created for you during the provisioning of your GCP account).

2. From GCP console API Library (https://console.cloud.google.com/apis), search for the following APIs and enable them (if they haven’t been enabled yet):

- Enable Compute Engine API
- Enable Cloud Resource Manager API
- Enable Cloud DNS API
- Enable Service Usage API
- Google Cloud APIs
- Identity and Access Management (IAM) API
- IAM Service Account Credentials API (may already be enabled when account was provisioned)
- Service Management API (may already be enabled when account was provisioned)
- Google Cloud Storage JSON API (may already be enabled when account was provisioned)
- Cloud Storage may already be enabled when account was provisioned)

### Register a new domain, if needed

If you don't already own a domain name provided by GCP that you can use for this enablement, then you need to register a new domain. If you already own a domain name provided by GCP, then skip this section.

**NOTE**: Your domain provider must be GCP for this enablement. The following instructions depend on this.

3. Head over to [Google Cloud Domains](https://console.cloud.google.com/net-services/domains/registrations) and register a new domain for you to use during this enablement. For the domain creation, select these options:

- Select "Use Cloud DNS (Recommended)" for DNS Configuration and accept the default “Cloud DNS Zone”
- Leave the "Privacy protection on" checked
- Fill out your contact details

For this enablement, we will assume that the registered domain is **ocpgitlab.com**, but yours will be different.

### Creating a DNS Zone

4. Go to [Google Cloud DNS](https://console.cloud.google.com/net-services/dns/zones)

5. Create a zone, e.g. ocpgitlab-com, in Google Cloud DNS under your Google project. **NOTE:** if you registered a domain following the steps from the previous section, this zone will have been created for you already.

### Cloning the gitlab-operator project

6. GitLab Engineering has put together a script that automates the creation of an OCP cluster on GCP. Let's clone their project to your $HOME directory on your local drive. Execute the following command from your $HOME directory in a command window on your laptop/desktop:

> clone git@gitlab.com:gitlab-org/cloud-native/gitlab-operator.git

### Create a Google Cloud service account

7. Create Google Cloud Service account with appropriate permission in your Google project by following these [instructions](https://docs.openshift.com/container-platform/4.5/installing/installing_gcp/installing-gcp-account.html#installation-gcp-service-account_installing-gcp-account). Attach all roles marked as Required in that document.
Once the Service Account is created, download it as JSON, name it `gcloud.json` place this file in the root directory of the cloned repository on your laptop/desktop.

### Download OpenShift installer and tools

8. Download the OpenShift installer, Pull secret, and Command line interface from the OpenShift installer for GCP [site](https://cloud.redhat.com/openshift/install/gcp/installer-provisioned)

9. Unzip the OpenShift installer and CLI and place openshift-install and oc in your PATH by entering these commands from a command window on your laptop/desktop:
```
gunzip ~/Downloads/openshift-install-mac.tar.gz
gunzip ~/Downloads/openshift-client-mac.tar.gz
mkdir $HOME/oc-install && cd $HOME/oc-install && tar -xvf ~/Downloads/openshift-install-mac.tar
mkdir $HOME/oc-client && cd $HOME/oc-client && tar -xvf ~/Downloads/openshift-client-mac.tar
export PATH=$PATH:$HOME/oc-client:$HOME/oc-install
```	

10. Copy the pull_secret from step 8 above and paste it into a file named pull_secret in the root directory of the cloned repository on your laptop/desktop.

11. Change directory to where you cloned the repository from step 6 above by entering these commands from a command window on your laptop/desktop:

> cd $HOME/gitlab-operator

12. Create and set the following environment variables from a command window on your laptop/desktop:

| Variable | Value to use |
| ---      | ---      |
| BASE_DOMAIN | This is the domain you created in step 3, OR the domain name provided by GCP that you already own |
| GCP_PROJECT_ID | This is the name of your GCP project. This project was created for you during the provisioning of your GCP account via the [GitLab Cloud Sandbox](https://gitlabsandbox.cloud) |
| GCP_REGION | A GCP region where you'd like the OCP to be deployed to, e.g. `us-east1` |
| CLUSTER_NAME   | A string that will be used by the script to name your OCP cluster |

Here are examples of how to define these environment variables (notice that the values of your environment variables will be different):

```
export BASE_DOMAIN=“ocpgitlab.com”
export GCP_PROJECT_ID=“csaavedra-f609ce23”
export GCP_REGION="us-east1"
export CLUSTER_NAME="csaavedra"
```

**NOTE**: The variables CLUSTER_NAME and BASE_DOMAIN are combined to build the domain name for the cluster. Using the example values above, the OCP cluster domain name would be csaavedra.ocpgitlab.com

13. Before you run the install script, you need to ensure you have “wget”

> type wget

If the command above returns “wget not found” then install wget by running the following command from a command window on your laptop/desktop:

> brew install wget

### Running the script to install OCP on GCP

14. Finally, run the script to create the OpenShift cluster on GCP by entering the following command from the same command window where you defined the environment variables from step 12 above"

> ./scripts/create_openshift_cluster.sh

**NOTE**: The script will output the URL for the OCP console as well as the username and password to use to log in to it.

Here's a sample output from running the previous script:

```
csaavedra@Cesars-MacBook-Pro gitlab-operator % ./scripts/create_openshift_cluster.sh
Verifying requirements for OpenShift 4.7.17
./scripts/create_openshift_cluster.sh: line 70: type: bin/openshift-install-4.7.17: not found
--2021-10-04 21:58:38--  https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.7.17/openshift-install-mac.tar.gz
Resolving mirror.openshift.com (mirror.openshift.com)... 54.173.18.88, 54.172.173.155
Connecting to mirror.openshift.com (mirror.openshift.com)|54.173.18.88|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 96559512 (92M) [application/x-gzip]
Saving to: ‘tmp-cluster/openshift-install.tar.gz’

tmp-cluster/openshift-inst 100%[=====================================>]  92.09M  21.2MB/s    in 6.5s    

2021-10-04 21:58:44 (14.2 MB/s) - ‘tmp-cluster/openshift-install.tar.gz’ saved [96559512/96559512]

./scripts/create_openshift_cluster.sh: line 70: type: bin/oc-4.7.17: not found
--2021-10-04 21:58:45--  https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.7.17/openshift-client-mac.tar.gz
Resolving mirror.openshift.com (mirror.openshift.com)... 54.172.173.155, 54.173.18.88
Connecting to mirror.openshift.com (mirror.openshift.com)|54.172.173.155|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 23727984 (23M) [application/x-gzip]
Saving to: ‘tmp-cluster/oc.tar.gz’

tmp-cluster/oc.tar.gz      100%[=====================================>]  22.63M  2.91MB/s    in 7.3s    

2021-10-04 21:58:53 (3.08 MB/s) - ‘tmp-cluster/oc.tar.gz’ saved [23727984/23727984]

Rendering install-config file
Creating cluster 'csaavedra'
INFO Credentials loaded from environment variable "GOOGLE_CREDENTIALS", content <redacted> 
INFO Consuming Install Config from target directory 
INFO Creating infrastructure resources...         
INFO Waiting up to 20m0s for the Kubernetes API at https://api.csaavedra.ocpgitlab.com:6443... 
INFO API v1.20.0+2817867 up                       
INFO Waiting up to 30m0s for bootstrapping to complete... 
INFO Destroying the bootstrap resources...        
INFO Waiting up to 40m0s for the cluster at https://api.csaavedra.ocpgitlab.com:6443 to initialize... 
INFO Waiting up to 10m0s for the openshift-console route to be created... 
INFO Install complete!                            
INFO To access the cluster as the system:admin user when using 'oc', run 'export KUBECONFIG=/Users/csaavedra/gitlab-operator/install-csaavedra/auth/kubeconfig' 
INFO Access the OpenShift web-console here: https://console-openshift-console.apps.csaavedra.ocpgitlab.com 
INFO Login to the console with user: "kubeadmin", and password: "I8U8C-rmPrm-v5ZEQ-2afKh" 
INFO Time elapsed: 28m39s                         
Installing cert-manager
Installing helm to local directory bin/
--2021-10-04 22:27:36--  https://get.helm.sh/helm-v3.6.3-darwin-amd64.tar.gz
Resolving get.helm.sh (get.helm.sh)... 152.195.19.97
Connecting to get.helm.sh (get.helm.sh)|152.195.19.97|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 14356954 (14M) [application/x-tar]
Saving to: ‘STDOUT’

-                          100%[=====================================>]  13.69M  28.5MB/s    in 0.5s    

2021-10-04 22:27:37 (28.5 MB/s) - written to stdout [14356954/14356954]

Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "jetstack" chart repository
...Successfully got an update from the "gitlab" chart repository
...Successfully got an update from the "bitnami" chart repository
Update Complete. ⎈Happy Helming!⎈
Error: Kubernetes cluster unreachable: Get "http://localhost:8080/version?timeout=32s": dial tcp [::1]:8080: connect: connection refused
csaavedra@Cesars-MacBook-Pro gitlab-operator % echo $PATH
/Users/csaavedra/google-cloud-sdk/bin:/Users/csaavedra/.rbenv/shims:/Users/csaavedra/.nvm/versions/node/v12.4.0/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Users/csaavedra/terraform_1.0.5:/Users/csaavedra/oc-client:/Users/csaavedra/oc-install
csaavedra@Cesars-MacBook-Pro gitlab-operator %
```

### Setting KUBECONFIG to be able to run kubectl commands

15. To be able to run `kubectl` commands for the newly installed OCP cluster, you need to set your KUBECONFIG environment variable by entering the following command from the same command window:

> export KUBECONFIG=/Users/csaavedra/gitlab-operator/install-csaavedra/auth/kubeconfig

### Logging to the OCP console

16. To log in to the OCP Console, open a browser window and point it to the https address indicated in the script output above. Use the username and password indicated by the script.

OpenShift Container Platform is now installed on GCP.

## Installing the GitLab Kubernetes Operator on OCP

Let's go over the steps to install the GitLab Kubernetes Operator on the OpenShift Container Platform you provisioned in the previous sections.

> **NOTE**: Please refer to the [GitLab Kubernetes Operator project](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator) for more in-depth information.

### Install cert-manager on OCP

The GitLab Kubernetes Operator requires cert-manager to be installed and running on your OCP cluster. The installation of OCP you performed above did not include the installation of cert-manager so we need to do this now.

17. From the OCP console, click on the **Operators > OperatorHub** on the left vertical navigation bar. Then search for "cert-manager" in the Operator Hub by using the search field on the screen. Lastly, install the **cert-manager** operator provided by Jetstack by clicking on its tile:

**NOTE**: accept all the defaults during installation process.

---

<img src="platforms/images/ocp/0-ocp-operator-hub.png" width="50%" height="50%">

---

18. Once the operator has been installed, it needs to be instantiated on the OCP cluster. The last screen of the **cert-manager** installation process will display its operator page, which will have a link **Create instance**. Click on this link to instantiate an instance of the **cert-manager** on the OCP cluster. Alternatively, you can head to the **Installed Operators** screen by clicking **Operators > Installed Operators** on the left vertical navigation bar on the OCP console, and then click on the **cert-manager** operator name to get to its operator page from which you can click its link **Create instance**:

**NOTE**: accept all the defaults during instantiation process.

---

<img src="platforms/images/ocp/1-cert-mgr-create-instance.png" width="50%" height="50%">

---

### Create the gitlab-system namespace

The GitLab Kubernetes Operator and GitLab instance will all use the gitlab-system namespace, which needs to be created at this point. On a command window on your laptop/desktop, enter the following command:

> kubectl create namespace gitlab-system

### Applying the GitLab Kubernetes Operator Custom Resource Definition (CRD) to OCP

19. Reusing one of your open command windows (if none are open, open a new command window on your laptop/desktop), define the following environment variables in the command window:

```
export GL_OPERATOR_VERSION="0.1.0" 
export PLATFORM="openshift"
```

| Variable | Value to use |
| ---      | ---      |
| GL_OPERATOR_VERSION   | This is version of the GitLab K8s Operator you'd like to use. To get the latest version, check out the [GitLab Operator releases page](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/releases) |
| PLATFORM   | The [GitLab Kubernetes Operator project releases](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/releases) currently support vanilla kubernetes and OpenShift. Set this variable to **openshift** for this enablement |

20. In the same command window from the previous step, enter the following command:

> kubectl apply -f https://gitlab.com/api/v4/projects/18899486/packages/generic/gitlab-operator/${GL_OPERATOR_VERSION}/gitlab-operator-${PLATFORM}-${GL_OPERATOR_VERSION}.yaml

This command applies the appropriate GitLab Kubernetes Operator CRD to your OCP cluster and creates two instances of pod **gitlab-controller-manager** under the **gitlab-system** namespace. To watch the progress of the command above, you can open a new command window on your laptop/desktop and enter the following command:

> kubectl get pods -n gitlab-system --watch

### Create a Custome Resource (CR) for the GitLab instance

We now need to create a CR that leverages the already installed GitLab Kubernetes Operator. This CR provides the information the the Operator needs to create an instance of GitLab on OCP.

21. Using your favorite editor, create a new file called **mygitlab.yaml** in the root directory of the cloned repository on your laptop/desktop. . Then paste the following content into file **mygitlab.yaml**:

```
apiVersion: apps.gitlab.com/v1beta1
kind: GitLab
metadata:
  name: gitlab
spec:
  chart:
    version: "5.3.0"
    values:
      global:
        hosts:
          domain: apps.[CLUSTER_NAME].[BASE_DOMAIN]
        ingress:
          configureCertmanager: true
      certmanager-issuer:
        email: [YOUR_GITLAB_EMAIL]
```

**NOTE**: In the snippet above, replace CLUSTER_NAME and BASE_DOMAIN with the values from step 12 above, and YOUR_GITLAB_EMAIL with your GitLab email.

Save the file and exit your editor.

### Applying the CR to OCP

22. To tell the GitLab Kubernetes Operator to instantiate an instance of GitLab, we need to apply the CR to OCP by entering the following in a command window on your laptop/desktop:

> kubectl -n gitlab-system apply -f ./mygitlab.yaml

To watch the progress of the command above, you can open a new command window on your laptop/desktop and enter the following command:

> kubectl get pods -n gitlab-system --watch

The instantiation of a GitLab instance should take a few minutes, i.e. 5 to 10 minutes.

### Identifying the external IP of your GitLab instance

23. Once the GitLab instance is up and running on OCP, you need to obtain its external IP address. You do this by entering the following command:

> kubectl -n gitlab-system get services -o wide gitlab-nginx-ingress-controller

Make a note of the external IP address.

### Adding A records in Google Cloud DNS

24. Open your browser window and navigate to [Google Cloud DNS](https://console.cloud.google.com/net-services/dns/zones)

25. Click on the zone name you created in step 5 above, e.g. ocpgitlab-com, yours will be different.

**NOTE**: Ensure that you click on the entry with **Zone type** **Public**. There is another Zone that is **Private** that was created by the installation of the OCP cluster automation, whose description is **Managed by Terraform**. You want to add the A records to the **Public** zone, NOT the **Private** one.

26. Once in the Zone Details window, proceed to create three A-type records by clicking on **+ ADD RECORD SET** at the top of the screen, using the following information (leave the default for any unmentioned field):

| Variable | Value to use |
| ---      | ---      |
| DNS Name   | minio.apps.[CLUSTER_NAME], where [CLUSTER_NAME] is the name of your cluster from step 12 above |
| IPv4 Address   | the external IP address for the GitLab instance you obtained from step 23 above |

| Variable | Value to use |
| ---      | ---      |
| DNS Name   | registry.apps.[CLUSTER_NAME], where [CLUSTER_NAME] is the name of your cluster from step 12 above |
| IPv4 Address   | the external IP address for the GitLab instance you obtained from step 23 above |

| Variable | Value to use |
| ---      | ---      |
| DNS Name   | gitlab.apps.[CLUSTER_NAME], where [CLUSTER_NAME] is the name of your cluster from step 12 above |
| IPv4 Address   | the external IP address for the GitLab instance you obtained from step 23 above |

Once you create these A records, wait a couple of minutes for these to propagate on the internet DNS servers.

### Obtaining the GitLab instance root password

We need to get the GitLab root password so that we can log in to our newly created GitLab instance, which is already up and running on OCP.

27. To obtain the password of the root GitLab user, enter the following from a command window on your laptop/desktop:

> kubectl -n gitlab-system get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo

Make a note of the password.

## Log in to the GitLab instance

28. Open a browser window and navigate to **gitlab.apps.[CLUSTER_NAME].[BASE_DOMAIN]**, e.g. gitlab.apps.csaavedra.ocpgitlab.com, yours will be different. The GitLab log-in window should appear. Enter **root** in the **Username or email** field, and the password from step 27 above in the **Password** field.

You should be successfully logged in to the newly created GitLab instance running on OCP!

### Some useful kubectl commands that can be used during this enablement

```
kubectl get pods --all-namespaces | more

kubectl get namespaces | grep gitlab-system

kubectl get namespaces | grep gitlab-system

kubectl get pods -n gitlab-system --watch

kubectl -n gitlab-system get services -o wide gitlab-nginx-ingress-controller

kubectl get ingress -n gitlab-system

kubectl -n gitlab-system get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo
```

### References

The sources for this set of instructions are:

- [GitLab Operator Installation](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/blob/master/doc/installation.md)
- [OpenShift Cluster Setup](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/blob/master/doc/openshift-cluster-setup.md#create-a-google-cloud-gcp-service-account)
