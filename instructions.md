# The GitLab Kubernetes Operator

The GitLab Operator allows you to install and run an instance of GitLab in a vanilla Kubernetes or OpenShift cluster. The Operator goes beyond the standard capability of the GitLab Helm Chart by running continuously to monitor and update your GitLab instance automating day 2 operations such as upgrading components, application reconfiguration, and autoscaling.

## K8s Operator and Helm

Helm is a package manager for Kubernetes, similar to npm for Node. It can install, upgrade and uninstall workloads on K8s via charts, which reside in a chart repository. These activities are usually referred to as day-1 operations.

A Kubernetes Operator is a pattern that encapsulates operator knowledge into it. It extends the Kubernetes API to automate the management of the entire lifecycle of a particular application, such as its creation, configuration, management of data integrity, scaling it up or down, or automatic recovery from a failure. These activities are also known as day-2 activities. 

Helm and K8s Operators are complementary technologies. For example, you can have helm charts to deploy operators and you can have operators that make use of the Helm API.

## Installing the GitLab Kubernetes Operator

Here are the instructions to install the GitLab K8s Operator on different platforms:

1. [Installing the GitLab K8s Operator on a local **kind** K8s cluster](https://gitlab.com/tech-marketing/k8s-operator/sa-tam-enablement/-/blob/main/platforms/kind_install.md). This gives you a local environment for demoing the GitLab K8s Operator.
2. [Installing the GitLab K8s Operator on Red Hat OpenShift Container Platform](https://gitlab.com/tech-marketing/k8s-operator/sa-tam-enablement/-/blob/main/platforms/ocp_install.md)
<!--
3. Installing the GitLab K8s Operator on GKE
-->
